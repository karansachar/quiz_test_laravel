<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quiz;
use App\QuizOptions;

class QuizController extends Controller
{
    //
	public function index()
    {
		$Quiz = array();
		$allqns = array();
		$allans = array();
		$QuizQns = Quiz::all();
		$QuizAns = Quiz::join('quiz_options', 'quiz.id', '=', 'quiz_options.quiz_id')->get();
		foreach($QuizQns as $qns ){
			$array_qns = array();
			$array_qns['question_name'] = $qns->question_name;
			$array_qns['question_id'] = $qns->id;
			array_push($allqns, $array_qns);
		}
		
		$i = 0;
		foreach($allqns as $questions){
			$myans = array();
			
			$values = QuizOptions::where('quiz_id', $questions['question_id'])->get();
			foreach($values as $ans ){
				$array_ans = array();
				$array_ans['option_id'] = $ans->id;
				$array_ans['options'] = $ans->options;
				array_push($myans, $array_ans);
			}
			$Quiz[$i]['questions']['question_id'] = $questions['question_id'];
			$Quiz[$i]['questions']['question_name'] = $questions['question_name'];
			$Quiz[$i]['questions']['option'] = $myans;
			
			$i++;
		}
		return view('take_quiz',compact('Quiz'));
	}
	
	public function postResult(Request $request)
    {
		$showAns = array();
		$showAnss = array();
		$allPostData = $request->all();
		$calc = count($allPostData)-1;
		$corect=0;
		$incorect=0;
		for($k=0;$k<$calc;$k++){
			$status = "Correct";
			$class = "success";
			$trueVal = Quiz::where('id', '=', $k+1)->first();
			$corectVal = QuizOptions::where(['quiz_id'=>$k+1])->offset($trueVal->answer - 1)->limit(1)->get();
			$incorecrVal = QuizOptions::where(['quiz_id'=>$k+1])->offset($allPostData['option_'.$k] - 1)->limit(1)->get();

			if($allPostData['option_'.$k] != $trueVal->answer){
				$status = "Incorrect";
				$class = "danger";
				$incorect++;
			}else{
				$corect++;
			}
			$showAns[$k]['status'] = $status;
			$showAns[$k]['class'] = $class;
			$showAns[$k]['question'] = $trueVal->question_name;
			$showAns[$k]['your_answer'] = $incorecrVal[0]->options;
			$showAns[$k]['correct_answer'] = $corectVal[0]->options;
		}
		$pass_fail = round($corect / ($calc / 100),2);
		$showAnss['answers'] = $showAns;
		$extradetails = array("correct"=>$corect,"incorrect"=>$incorect,"result"=>$pass_fail);
		$showDetails = array_merge($extradetails,$showAnss);
		
		return view('show_result',compact('showDetails'));
	}
}
