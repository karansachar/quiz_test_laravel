<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Quiz extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'quiz';
    protected $fillable = [
        'question_name', 'answer', 'options'
    ];
	
	public function quiz_options(){
		return $this->belongsTo(QuizOptions::class, 'quiz_id');
    }

}
