<?php

use Illuminate\Database\Seeder;

class QuizAnswerTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		$optionsList = [
			"Web Health Organisation",
			"World Healing Organisation",
			"Web Health Overall",
			"World Health Organisation",
			"1992",
			"2019",
			"2020",
			"2005",
			"Sonia Gandhi",
			"Narendra Modi",
			"Mahatama Gandhi",
			"Suraj Modi",
			"Amritsar",
			"New Delhi",
			"Goa",
			"Assam",
			"Srinagar",
			"Assam",
			"Gujarat",
			"Mumbai",
			"Arunachal Pradesh",
			"Bihar",
			"Punjab",
			"Maharashtra",
			"Atal Bihari Vajpayee",
			"Mahatma Gandhi",
			"Rajiv Gandhi",
			"Jawaharlal Nehru",
			"Kerala",
			"Nagaland",
			"Sikkim",
			"West Bengal",
			"Sachin Tendulkar",
			"Virat Kholi",
			"Pranav Dhanawade",
			"Shikhar Dhawan",
			"Amitabh Bachchan",
			"Shah Rukh Khan",
			"Nawazuddin Siddiqui",
			"Paresh Rawal"
		];
		$question_id = [ 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 10];
		$item = 0;
		foreach ($optionsList as $options) {
			DB::table('quiz_options')->insert([
			   'quiz_id' => $question_id[$item],
			   'options' => $optionsList[$item],
			   'created_at' => now()
		   ]);
		   $item++;
		}
    }
}
