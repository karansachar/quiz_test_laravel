<?php

use Illuminate\Database\Seeder;

class QuizTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		$questions = [
			"What is Full form of WHO",
			"In Which Year, lockdown held all over the world.",
			"Who is Prime Minister of India?",
			"What is Captital of the India?",
			"Where is Dal Lake in the India?",
			"Amritsar is situated In which State?",
			"Who is the First Prime Minister of India?",
			"Which state launch a project that aims to provide free internet access to the poor in the State?",
			"Name the first cricketer to score 1000 runs in an innings in any competitive match.",
			"Who is the King of Bollywood?"
		];
		$answers = [ 4, 3, 2, 2, 1, 3, 4, 1, 3, 2 ];
		$item = 0;
		foreach ($questions as $question) {
			DB::table('quiz')->insert([
			   'question_name' => $questions[$item],
			   'answer' => $answers[$item],
			   'created_at' => now()
		   ]);
		   $item++;
		}
	
    }
}
