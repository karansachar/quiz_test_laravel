<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
		<title> Welcome General Test Quiz</title>
		<link rel="stylesheet" href="{{URL::asset('assets/css/multi-form.css')}}">
    </head>
    <body>
	
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<div class="card">
						
						<form action="{{ route('submitresult') }}" method="POST" class="wizard" id="quizform">
							@csrf
							<div class="card-body">
								@if (Session::has('message'))
									<div class="alert alert-info">{{ Session::get('message') }}</div>
								@endif
								
								<?php $total = count($showDetails['answers']); ?>
								<h3>Result</h3>
								@foreach($showDetails['answers'] as $showDetail)
									<div class="panel-primary">
										<div class="panel-heading">
											<?php $myvalS = $loop->index; ?>
											<h3><?php echo $myvalS+1;?> {{ $showDetail['question'] }}</h3>
										</div>
										<div class="panel-body">
											<ul class = "list-group" style="margin-bottom:2%">
												<li class = "list-group-item">
													<span style="font-size:20px">Your Answer</span><br>
													<button class="btn btn-{{$showDetail['class']}}">{{$showDetail['your_answer']}}</button>
													<br><span style="font-size:20px">Correct Answer</span><br>
													<button class="btn btn-success">{{$showDetail['correct_answer']}}</button>
												</li>
											</ul>
										</div>
									</div>
								@endforeach
								
							</div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
    </body>
</html>
