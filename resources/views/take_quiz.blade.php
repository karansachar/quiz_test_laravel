<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
		<title> Welcome General Test Quiz</title>
		<link rel="stylesheet" href="{{URL::asset('assets/css/multi-form.css')}}">
		<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
		
		<script type="text/javascript" src="{{URL::asset('assets/js/multi-form.js')}}"></script>
    </head>
    <body>
	
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<div class="card">
						
						<form action="{{ route('submitresult') }}" method="POST" class="wizard" id="quizform">
							@csrf
							<div class="card-body">
								@if (Session::has('message'))
									<div class="alert alert-info">{{ Session::get('message') }}</div>
								@endif
								
								<?php $total = count($Quiz); ?>
								
								@foreach($Quiz as $Quizs)
									
								<div class="tab">
									<h4> {{ $loop->index+1}} out of {{ count($Quiz)}}</h4>
									<div class="panel-primary">
										<div class="panel-heading">
											<?php $myvalS = $loop->index; ?>
											<h3>{{ $Quizs['questions']['question_name'] }}</h3>
										</div>
										<div class="panel-body">
											<ul class = "list-group">
												<?php $anscheck = 0; ?>
												@foreach($Quizs['questions']['option'] as $options )
												<?php $anscheck++; ?>
												<li class = "list-group-item">
													  <div class="radio">
														<input type="radio" required id="option_<?php echo $myvalS;?>" name="option_<?php echo $myvalS;?>" class="quizopt" value="<?= $anscheck;?>"/>
														<label for="radio">
															{{ $options['options'] }}
														</label>
													</div>
												</li>
												@endforeach
											   </ul>
										</div>
									</div>
									<p class="error" id="error_<?php echo $myvalS;?>"></p>
									<div style="overflow:auto;">
										<div style="float:right; margin-top: 5px;">
										<?php if($myvalS+1 < $total){ ?>
											<button type="button" class="previous">Previous</button>
											<button type="button" class="next">Next</button>
										<?php }else{ ?>
											<button type="button" class="submit">Save</button>
										<?php }?>
										</div>
									</div>
								</div>
								@endforeach
								
							</div>
						</form>
						
					</div>
				</div>
			</div>
		</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#quizform").multiStepForm({
				// defaultStep:0,
				beforeSubmit : function(form, submit){
					console.log("called before submiting the form");
					console.log(form);
					console.log(submit);
				}
			}).navigateTo(0);

		});
	</script>
    </body>
</html>
